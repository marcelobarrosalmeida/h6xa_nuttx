
#include <nuttx/config.h>

#if defined(CONFIG_MBED_PROFILE_SUITE_B)
#include "mbedtls/configs/config-suite-b.h"
#elif defined(CONFIG_MBED_PROFILE_SUITE_B_FS)
#include "mbedtls/configs/config-suite-b-fs.h"
#elif defined(CONFIG_MBED_PROFILE_CCM_PSK_TLS_1_2)
#include "mbedtls/configs/config-ccm-psk-tls1_2.h"
#elif defined(CONFIG_MBED_PROFILE_MINI_TLS_1_1)
#include "mbedtls/configs/config-mini-tls1_1.h"
#elif defined(CONFIG_MBED_PROFILE_PICO_COIN)
#include "mbedtls/configs/config-picocoin.h"
#elif defined(CONFIG_MBED_PROFILE_STM)
#include "mbedtls/configs/config-stm.h"
#elif defined(CONFIG_MBED_PROFILE_THREAD)
#include "mbedtls/configs/config-thread.h"
#elif defined(CONFIG_MBED_PROFILE_FULL)
#include "mbedtls/configs/config-full.h"
#elif defined(CONFIG_MBED_PROFILE_CUSTOM)
#include "mbedtls/configs/config-custom.h"
#else
#error "mbed TLS configuration file was not set"
#endif

